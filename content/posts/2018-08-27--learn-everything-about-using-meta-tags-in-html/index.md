---
title: "Learn everything about using meta tags in html"
date: 2018-08-27T18:00:00.875Z
categories:
    - html
tags: [css,codingnewbie]
author: Madhusudhan R
draft: true
---
Today, we are going to learn about the meta tags using in html inside the head tag.

When you start learning HTML you will come across <meta/> tag which you write inside the <head></head> tag. You will see different values defined in them when you inspect different websites. Often they are confusing inspite the tags themselves explain what they are defined for!

To clear all the doubts first you need to know **Why to use a meta tag?**

A meta tag provides the metadata about the html document.

A metadata is data or information about the data.

For analogy think, that you have placed some items at your home in different places like bedroom, kitchen,attic etc. The list(data) is very huge and to know information about this data (where you have kept a particular item) you prepare one more list(metadata). With that list you will be able to know where you have kept a thing? and how to get there?

<div class="highlight highlight-success">
Meta data is defined not for the humans but for the machine/browsers/search engines to parse and take action.
</div>

Let's look at an example
```html
<html>
  <head>
    <meta charset="utf8"/>
  </head>
</html>
```

In the above example you can see we have defined the charset a property and utf8 as value.

The information is always passed as name-value pairs inside meta tags.

As the browser parses the html on the page it sees the meta tags and performs actions as the tags define.